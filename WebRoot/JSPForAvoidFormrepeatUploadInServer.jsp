<%@page import="com.hww.util.TokenProcessor"%>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>登陆页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

  </head>
  
  <body>
  	<!-- 这是脚本元素的脚本段 -->
	<%
		TokenProcessor processor = TokenProcessor.getInstance();
		String token = processor.getToken(request);
	 %>

	<form action="handlerserver" method="post"name="theFormServer">
		<table>
			<tr>
				<td>用户名：</td>
				<td><input typ="text" name="username"></td>
			</tr>
			
			<tr>
				<td>邮件地址</td>
				<td>
					<input type="text" name="email">
					<!-- value值是脚本元素的表达式 -->
					<input type="hidden" name="com.hww.token" value="<%=token%>"/>
				</td>
			</tr>
			
			<tr>
				<td><input type="reset" value="重填"></td>
				<td><input type="submit" name="submit" value="提交" onchange="checkSubmit();"/></td>
			</tr>
		</table>
	</form>
  </body>
</html>
