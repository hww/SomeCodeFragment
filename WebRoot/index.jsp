<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=GBK"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>登陆页面</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script language="javascript">
<!-- 声明一个检测是否提交的变量，默认值为true -->
	var checkSubmitFlag = true;

	function checkSubmit() {
		if (true == checkSubmitFlag) {
			//document.theForm.btnSubmit.disables = true;
			document.theForm.submit();
			checkSubmitFlag = false;
		}else{
			alert("你已经提交了表单")
		}
	}
</script>

</head>

<body>
	<form method="post" action="handler" name="theForm">
		<table>
			<tr>
				<td>用户名：</td>
				<td><input type="text" name="username">
				</td>>
			</tr>

			<tr>
				<td>邮件地址：</td>
				<td><input type="text" name="email">
				</td>>
			</tr>

			<tr>
				<td><input type="reset" value="重填"></td>>
				<td><input type="submit" name="ubtnSubmit" value="提交" onClick="checkSubmit();"/></td>>
			</tr>
		</table>
	</form>
</body>
</html>
