/**
 * 
 */
package com.hww.DecoratorPattern;

/**
 * 充当concreteComponent的角色
 * @author:Huangwenwei
 * @date:2014-5-4 
 * @time:上午11:18:23
 */
public class man implements Person{
	public void eat(){
		System.out.println("男人正在吃");
	}
}
