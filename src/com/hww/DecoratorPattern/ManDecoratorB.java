/**
 * 
 */
package com.hww.DecoratorPattern;

/**
 * 充当ConcreteDecorator的角色
 * @author:Huangwenwei
 * @date:2014-5-4 
 * @time:上午11:29:37
 */
public class ManDecoratorB extends Decorator {

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		super.eat();
		System.out.println("=============");
		System.out.println("ManDecoratorB类");
	}
}
