/**
 * 
 */
package com.hww.DecoratorPattern;

/**
 * 
 * 充当component的角色
 * @author:Huangwenwei
 * @date:2014-5-4 
 * @time:上午11:15:48
 */
public interface Person {
	void eat();
}
