/**
 * 
 */
package com.hww.DecoratorPattern;

/**
 * 
 * 测试类
 * @author:Huangwenwei
 * @date:2014-5-4 
 * @time:上午11:31:34
 */
public class test {

	/**
	 * @param args  
	 * return:void
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		man Man = new man();
		ManDecoratorA mdA = new ManDecoratorA();
		ManDecoratorB mdB = new ManDecoratorB();
		
		mdA.setPerson(Man);
		mdB.setPerson(mdA);
		mdB.eat();

	}

}
