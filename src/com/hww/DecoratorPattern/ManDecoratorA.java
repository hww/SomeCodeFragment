/**
 * 
 */
package com.hww.DecoratorPattern;

/**
 * 
 * 充当ConcreteDecorator的角色
 * @author:Huangwenwei
 * @date:2014-5-4 
 * @time:上午11:25:53
 */
public class ManDecoratorA extends Decorator {

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		super.eat();
		reEat();
		System.out.println("ManDecoratorA类");
		
	}
	
	public void reEat(){
		System.out.println("再吃一餐");
	}
	
}
