/**
 * 
 */
package com.hww.DecoratorPattern;

/**
 * 
 * 充当decorator的角色
 * @author:Huangwenwei
 * @date:2014-5-4 
 * @time:上午11:22:47
 */
public abstract class Decorator implements Person {
	
	protected Person person;
	

	public void setPerson(Person person) {
		this.person = person;
	}


	/* (non-Javadoc)
	 * @see com.hww.DecoratorPattern.Person#eat()
	 */
	@Override
	public void eat() {
		// TODO Auto-generated method stub
		person.eat();
	}

}
