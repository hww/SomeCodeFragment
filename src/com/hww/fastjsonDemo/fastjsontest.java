/**
 * 
 */
package com.hww.fastjsonDemo;

import com.alibaba.fastjson.JSON;

/**
 * @author:Huangwenwei
 * @date:2014-5-18
 * @time:下午4:13:18
 */
public class fastjsontest {
	public static void main(String[] args) {
		group g = new group();
		g.setId(0L);
		g.setName("admin");
		
		user guestUser = new user();
		guestUser.setId(2L);
		guestUser.setName("guest");
		
		user rootUser = new user();
		rootUser.setId(3L);
		rootUser.setName("root");
		
		g.getUsers().add(guestUser);
		g.getUsers().add(rootUser);
		
		String jsonString = JSON.toJSONString(g);
		
		System.out.println(jsonString);
	}
}
