/**
 * 
 */
package com.hww.fastjsonDemo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author:Huangwenwei
 * @date:2014-5-18 
 * @time:下午4:10:43
 */
public class group {
	private Long id;
	private String name;
	private List<user> users = new ArrayList<user>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<user> getUsers() {
		return users;
	}
	public void setUsers(List<user> users) {
		this.users = users;
	}
}
