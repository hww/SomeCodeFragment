/**
 * 
 */
package com.hww.fastjsonDemo;

/**
 * @author:Huangwenwei
 * @date:2014-5-18 
 * @time:下午4:09:28
 */
public class user {
	private Long id;
	private String name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
