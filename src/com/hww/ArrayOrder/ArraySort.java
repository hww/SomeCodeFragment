package com.hww.ArrayOrder;

import java.util.Random;

/**
 * 排序测试类
 */
public class ArraySort {

	/**
	 * 初始化测试数组的方法
	 * @return 一个初始化好的数组
	 */
	public int[] createArray() {
		Random random = new Random();
		int[] array = new int[100];
		for (int i = 0; i < 100; i++) {
			array[i] = random.nextInt(100) - random.nextInt(100);// 生成两个随机数相减，保证生成的数中有负数
		}
		System.out.println("==========原始序列==========");
		printArray(array);
		return array;
	}
	/**
	 * 打印数组中的元素到控制台
	 * @param source
	 */
	public void printArray(int[] data) {
		for (int i : data) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
	/**
	 * 交换数组中指定的两元素的位置
	 * @param data
	 * @param x
	 * @param y
	 */
	private void swap(int[] data, int x, int y) {
		int temp = data[x];
		data[x] = data[y];
		data[y] = temp;
	}
	/**
	 * 随机生成新数组
	 * @return 数组
	 */
	public int[] createNewArray(int[] arr) {
		Random random = new Random();
		int[] array = new int[10];
		for (int i = 0; i < 10; i++) {
			array[i] = arr[random.nextInt(10)] ;// 生成随机数
		}
		System.out.println("==========生成新的原始序列==========");
		printArray(array);
		return array;
	}
	/**
	 * 直接选择排序法----选择排序的一种
	 * @param data要排序的数组
	 * @param sortType排序类型
	 * @return
	 */
	public void selectSort(int[] data, String sortType) {
		if (sortType.equals("asc")) { // 正排序，从小排到大
			int index;
			for (int i = 1; i < data.length; i++) {
				index = 0;
				for (int j = 1; j <= data.length - i; j++) {
					if (data[j] > data[index]) {
					index = j;
					}
				}
				// 交换在位置data.length-i和index(最大值)两个数
				swap(data, data.length - i, index);
			}
		} else if (sortType.equals("desc")) { // 倒排序，从大排到小
			int index;
			for (int i = 1; i < data.length; i++) {
				index = 0;
				for (int j = 1; j <= data.length - i; j++) {
					if (data[j] < data[index]) {
						index = j;
					}
				}
				// 交换在位置data.length-i和index(最大值)两个数
				swap(data, data.length - i, index);
			}
		} else {
			System.out.println("您输入的排序类型错误！");
		}
		printArray(data);// 输出直接选择排序后的数组值
	}
	
	/**
	 * 主测试方法
	 * @param args
	 */
	public static void main(String[] args) {
		ArraySort sortTest = new ArraySort();
		int[] array = sortTest.createArray();//创建数组
		int[] new_array = sortTest.createNewArray(array);
		
		System.out.println("==========排列后的数组==========");
		sortTest.selectSort(new_array, "asc");
		
		
		
	}
}
