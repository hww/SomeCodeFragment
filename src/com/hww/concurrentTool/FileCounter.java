/**
 * 
 */
package com.hww.concurrentTool;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author:Huangwenwei
 * @date:2014-5-25 
 * @time:下午2:53:22
 */
public class FileCounter {

	/**
	 * @param args  
	 * void
	 */
	public static void main(String[] args) {
		Path[] dirs = {
				//使用这份代码者需要修改一下的文件目录
				Paths.get("F:/TheDocumentsAboutSchool"),
				Paths.get("F:/Profession/database")
		};
		//利用ExecutorService的newFixedThreadPool方法，创建于dir中的目录数量相同的线程。每个目录一个线程
		ExecutorService executorService = Executors.newFixedThreadPool(dirs.length);
		Future<Long>[] results = new Future[dirs.length];
		
		//为每个目录创建一个FileCountTest，并将它传递给ExecutorService
		for(int i = 0;i<dirs.length;i++){
			Path dir = dirs[i];
			FileCountTest task = new FileCountTest(dir);
			//将一个Callable传递给ExecutorService的一个submit()方法
			//可以将一个Callable传递给ExecutorService的一个submit()方法，submit返回一个Future
			results[i] = executorService.submit(task);	
		}
		//输出结果
		for(int i= 0;i<dirs.length;i++){
			long fileCount = 0L;
			try{
				fileCount = results[i].get();
			}catch (InterruptedException | ExecutionException ex) {
				ex.printStackTrace();
			}
			System.out.println(dirs[i] + "contains " + fileCount + " files");
		}
		executorService.shutdownNow();
	}

}
