/**
 * 
 */
package com.hww.freemarker;

/**
 * @author:Huangwenwei
 * @date:2014-6-24
 * @time:下午12:49:30
 */
public class User {

	private String firstname;
	private String lastname;


	public User(String firstname, String lastname) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

}
