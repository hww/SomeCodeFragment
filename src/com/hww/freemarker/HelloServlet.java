/**
 * 
 */
package com.hww.freemarker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author:Huangwenwei
 * @date:2014-6-24
 * @time:下午12:51:26
 */
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static List<User> userlist = new ArrayList<User>();

	static {
		userlist.add(new User("Bill", "Gates"));
		userlist.add(new User("Steve", "Jobs"));
		userlist.add(new User("Larry", "page"));
		userlist.add(new User("Sergey", "Brin"));
		userlist.add(new User("Larry", "Ellison"));
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setAttribute("users", userlist);
		req.getRequestDispatcher("index.ftl").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");

		if (null != firstname && null != lastname && !firstname.isEmpty()
				&& !lastname.isEmpty()){
			synchronized (userlist) {
				userlist.add(new User(firstname,lastname));
			}
		}
		doGet(req, resp);

	}
}
