/**
 * 
 */
package com.hww.Singleton;

/**
 * @author:Huangwenwei
 * @date:2014-5-24
 * @time:下午5:15:44
 */
public class Singleton {

	// 使用一个变量来缓存曾经创建的实例
	private static Singleton instance;

	// 构造方法使用private修饰，隐藏该构造方法
	private Singleton() {}

	// 提供一个静态方法，用于返回Singleton实例，该方法可以加入自定义控制，保证只产生一个Singleton对象
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
	
	public static void main(String[] args) {
		// 创建Singleton对象不能通过构造方法，只能通过getInstance方法
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();

		System.out.println(s1 == s2);
	}
}
