/**
 * 
 */
package com.hww.Reflect;

/**
 * @author:huangwenwei
 * @date:2014-4-29 
 * @time:下午6:11:38
 */
public class SuperMan implements ActionInterface {
	
	private boolean blueBriefs;
	
	public void fly(){
		System.out.println("超人会飞~~");
	}
	
	public boolean isBlueBriefs(){
		return blueBriefs;
	}
	
	public void setBlueBriefs(boolean blueBriefs) {
		this.blueBriefs = blueBriefs;
	}

	/* (non-Javadoc)
	 * @see com.hww.Reflect.ActionInterface#walk(int)
	 */
	@Override
	public void walk(int m) {
		System.out.println("超人会走，走了..." + m +"米就停了。");
	}
}
