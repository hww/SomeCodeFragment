/**
 * 
 */
package com.hww.Reflect;

/**
 * @author:huangwenwei
 * @date:2014-4-29 
 * @time:下午6:07:48
 */
public class Person {
	private int age;
	private String name;
	/**
	 * 
	 */
	public Person() {
		super();
	}
	/**
	 * @param age
	 * @param name
	 */
	public Person(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
