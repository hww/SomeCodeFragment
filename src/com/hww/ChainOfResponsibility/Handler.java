/**
 * 
 */
package com.hww.ChainOfResponsibility;


/**
 * @author 黄文威
 *
 * @时间    2014-4-29 下午4:00:51
 */
public abstract class Handler {
	public abstract void dealIt(int free);
	
	Handler nexthandler;

	public Handler getHandler() {
		return this.nexthandler;
	}

	public void setHandler(Handler nexthandler) {
		this.nexthandler = nexthandler;
	}
}
