/**
 * 
 */
package com.hww.ChainOfResponsibility;

/**
 * @author 黄文威
 *
 * @时间    2014-4-29 下午4:00:36
 */
public class FirstHandler extends Handler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hww.ChainOfResponsibility.Handler#dealIt(int)
	 */
	@Override
	public void dealIt(int free) {
		if (free < 200) {
			System.out.println("I am first handler,I deal with less 200");
		} else {
			nexthandler = getHandler();

			if (null != nexthandler) {
				nexthandler.dealIt(free);
			}
		}
	}
}
