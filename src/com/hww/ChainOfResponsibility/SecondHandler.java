/**
 * 
 */
package com.hww.ChainOfResponsibility;
/**
 * @author 黄文威
 *
 * @时间    2014-4-29 下午4:00:45
 */
public class SecondHandler extends Handler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hww.ChainOfResponsibility.Handler#dealIt(int)
	 */
	@Override
	public void dealIt(int free) {
		if (free < 500) {
			System.out.println("I am second handler,I deal with less 500");
		} else {
			nexthandler = getHandler();

			if (null != nexthandler) {
				nexthandler.dealIt(free);
			}
		}
	}
	
	public static void main(String[] args) {
		Handler firsthandler = new FirstHandler();
		Handler secondhandler = new SecondHandler();
		
		firsthandler.setHandler(secondhandler);
		secondhandler.setHandler(null);
		
		firsthandler.dealIt(400);
	}
}
