package com.hww;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HandlerServlet extends HttpServlet {
	int count=0;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/html;charset=GBK");
		PrintWriter out = resp.getWriter();
		
		try{
			Thread.sleep(5000);
		}catch(InterruptedException e){
			System.err.println(e);
		}
		
		System.out.println("submit:" + count);
		if(count%2==1)
			count=0;
		else
			count++;
		out.println("success");
		out.close();
		
		
	}
}
