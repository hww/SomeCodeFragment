/**
 * 
 */
package com.hww.RandomStringUtil;

import org.apache.commons.lang.RandomStringUtils;

/**
 * @author:Huangwenwei
 * @date:2014-5-19 
 * @time:下午4:32:01
 */
public class RandomStringUtil {

	/**
	 * @param args  
	 * void
	 */
	public static void main(String[] args) {
		//create a 64 chars random string of number
		String result = RandomStringUtils.random(64,false,true);
		System.out.println("random = " + result);
		
		//creates a 64 chars length of random alphabetic string
		result = RandomStringUtils.randomAlphabetic(64);
		System.out.println("random = " + result);
		
		//creates a 32 chars length of random ascii string
		result = RandomStringUtils.randomAscii(32);
		System.out.println("random = " + result);
		
		//create a 32 chars length of string from the defined array of characters including numeric alphabetic characters
		result = RandomStringUtils.random(32,0,20,true,true,"qw32rfHIJK9iQ8Ud7h0x".toCharArray());
		System.out.println("random=" + result);
	}

}
